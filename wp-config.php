<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'php_usr');

/** MySQL database password */
define('DB_PASSWORD', 'wtfit@#16');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'big?AxfX$6^RbX3DYy8O0t{m8f+(u48@T>l2L1JYtp>B1Vj,ld&`9z#f&S3hH/c<');
define('SECURE_AUTH_KEY',  'Rbk;r>$AAb%:D@Dh` Yv7qi`_QR[R*$Buq$m+0.e$-0 %5hcGg8P<+sHr=)AdLhA');
define('LOGGED_IN_KEY',    'rj<J1h8k]Hxa=Jmr81_[^)pAdagyK;0,?0W1p!Jna0LMF^::<,=KNg9B#1NP:BU2');
define('NONCE_KEY',        'k?5>`wUYo29:|A[U|j*1[!tPvInk~GRk*KC,L9W9zkzy2TD&&EmERR jO?yS!}$2');
define('AUTH_SALT',        '_)qf3:pOKm;:1OP;w>!M>{/*|@.rL*;3`<,o>A<./|m:lu>[wL8Jg[ub<Kpe2 %4');
define('SECURE_AUTH_SALT', '!T)OwIQ*n<AL!AyEk$RT+k7cBqHILEl_8)(c+xM}8+}86d^5t]|OmVDt9y^!*NV=');
define('LOGGED_IN_SALT',   'sAI_pZA)SORB~z)kciMtV%$&Z{PlYM&SiR1aX:0:du<b(q^L~dmXuAfnI<9B+J7+');
define('NONCE_SALT',       'B^zPUr~jprC.Ad_ghN),f+6s,tbc^4`:qRL1^f% 5V7P@SS!xkyfngV]n+)/KWtU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
